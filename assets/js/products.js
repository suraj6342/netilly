$(function() {
	$( ".a" ).slider({
	range: true,
	  min: 0,
	max: 1000,
	labels: 7,
	values: [ 0, 1000 ],
	slide: function( event, ui ) {
		$('#min').val(ui.values[ 0 ]);
		$('#max').val(ui.values[ 1 ]);
		$('.price-min-span').html(ui.values[ 0 ]);
		$('.price-max-span').html(ui.values[ 1 ]);
	    $('.ui-slider-handle:first').html('<div class="tooltip top slider-tip current-time"><div class="tooltip-arrow"></div><div class="tooltip-inner actual-time ng-binding">' + ui.values[0] + ' tl'+'</div></div>');
		$('.ui-slider-handle:last').html('<div class="tooltip top slider-tip current-time"><div class="tooltip-arrow"></div><div class="tooltip-inner actual-time ng-binding">' + ui.values[1] + ' tl'+'</div></div>');
	},
	change: function(event, ui) {}
	})   
});

if($(window).width() >= 500){
	$('.list-btn').click(function(){
		$('.product-list-tab-main-inner').children().removeClass("col-sm-4");
		$('.product-list-tab-main-inner').children().addClass("col-sm-12");

		$('.product-list-tab-main-inner').children().children().removeClass("col-sm-12");
		$('.product-list-tab-main-inner').children().children().addClass("col-sm-4");
		$('.product-tab-bottom').hide();
		$('.list-product-tab-bottom').show();

	});	

	$('.grid-btn').click(function(){
		$('.product-list-tab-main-inner').children().addClass("col-sm-4");
		$('.product-list-tab-main-inner').children().removeClass("col-sm-12");

		$('.product-list-tab-main-inner').children().children().addClass("col-sm-12");
		$('.product-list-tab-main-inner').children().children().removeClass("col-sm-4");
		$('.product-tab-bottom').show();
		$('.list-product-tab-bottom').hide();

	});
}

$('.filter-btn').click(function(){
	$('.product-filter-tab').animate({left: "0%" });
});

$('.filter-close').click(function(){
	$('.product-filter-tab').animate({left: "-100%" });
});