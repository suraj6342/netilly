<?php
  include_once 'config/config.php';
  /* instantiate Database */
  $database   = new Database();
  $db         = $database->getConnection();

  $items = array();

  if($cart_id != '') {
    $cartQ = $db->query("SELECT * FROM cart WHERE id = '{$cart_id}'");
    $result = mysqli_fetch_assoc($cartQ);
    $items = json_decode($result['items'],true);
    $i = 1;
    $sub_total = 0;
    $item_count = 0;

  }

  function money($num){
    return '₹'.number_format($num,2);
  }

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Your Cart</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>

    <link rel="stylesheet" type="text/css" href="assets/css/cart.css?ver=<?php echo $randStr; ?>">
    
  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/mobile-sidebar.php'; ?>

    <div style="padding: 0 50px;" class="col-md-12 cart-section">
        <div class="row pad-lf-25">
            <h2 class="text-center">My Shopping Cart</h2><hr>
            <?php if($cart_id == ''): ?>
              <div class="bg-danger">
                <p class="text-center text-danger">
                  Your shopping cart is empty!
                </p>
              </div>
            <?php else: ?>
            <div style="overflow-x: auto;">
              <table class="table table-striped">
                <thead>
                  <th>#</th>
                  <th>Item</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Size</th>
                  <th>Image</th>
                  <th>Sub Total</th>
                </thead>
                <tbody>
                  <?php 
                  $s = array();
                    foreach ($items as $item) {
                      $product_id = $item['id'];
                      $sArray = array();

                      $productQ = $db->query("SELECT * FROM products WHERE id ='{$product_id}'");
                      $product = mysqli_fetch_assoc($productQ);
                      $sArray = explode(',', $product['sizes']);
                       
                       ?>
                       <tr>
                        <td><?=$i;?></td>
                        <td><?=$product['title'];?></td>
                        <td><?=money($product['price']);?></td>
                        <td>

                        <button class="btn menu-btn" onclick="update_cart('removeone','<?=$product['id'];?>','<?=$item['size'];?>');"> - </button>
                        &nbsp<?=$item['quantity'];?>&nbsp
                           
                        <button class="btn menu-btn" onclick="update_cart('addone','<?=$product['id'];?>','<?=$item['size'];?>');"> + </button>
                                                
                        </td>
                        <td><?=$item['size'];?></td>
                        <?php $photos = explode(',',$product['image']);?>
                        <td><img src="<?=$photos[0];?>" height="80px" width="80px" ></td>
                        <td><?=money($item['quantity'] * $product['price']);?></td>
                       </tr>

                       <?php
                       $i++; 
                       $item_count += $item['quantity'];
                       $sub_total += ($product['price'] * $item['quantity']);

                      }
                      $tax = TAXRATE * $sub_total;
                      $tax = number_format($tax,2);
                      $grand_total = $tax + $sub_total;

                      
                      ?>
                </tbody>
              </table>
            </div>
            <hr>
            <h2 class="text-center">Grand Total</h2><hr>
            <table class="table table-striped">
              <thead>
                <th>Total Items</th>
                <th>Sub Total</th>
                <th>Tax</th>
                <th>Grand Total</th>
              </thead>
              <tbody>
                <tr>
                  <td><?=$item_count;?></td>
                  <td><?=money($sub_total);?></td>
                  <td><?=money($tax);?></td>
                  <td class="bg-success"><?=money($grand_total);?></td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
    <?php endif; ?>

    <?php require_once 'includes/script.php'; ?>
    <script type="text/javascript" src="assets/js/cart.js?ver=<?php echo $randStr; ?>"></script>
    <script type="text/javascript">
        function update_cart(mode,edit_id,edit_size){
            var data = {"mode" : mode, "edit_id" : edit_id, "edit_size" :edit_size};
             jQuery.ajax({
              url : 'validation/update_cart.php',
              method : "post",
              data : data,
              success : function(){ location.reload(); 
              },
              error :function(){ alert("something went wrong");},
            });
        }
    </script>
  </body>
</html>