<?php

class Database{
 
    private $host = "localhost";
    private $db_name = "netilly";
    private $username = "root";
    private $password = "";

    public $conn;
    
        public function getConnection(){

	        global $connection;
	        $this->conn = null;
	        
	        $this->conn = new mysqli($this->host, $this->username, $this->password, $this->db_name);
	        if (mysqli_connect_errno()) {
	            $this->getMessage("Connect failed: %s\n", mysqli_connect_error(), 'Fatal');
	            exit();
	        }
	        return $this->conn;
    	}
}


?>