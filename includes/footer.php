<div class="col-xs-12 col-sm-12 no-pad-lr footer">
  <div class="col-xs-12 col-sm-12 footer-cta">
    <div class="col-xs-12 col-sm-4 cta-tab">
      <div class="col-xs-12 col-sm-12">
        <div class="col-xs-2 col-sm-2 no-pad-lr cta-icon">
          <span><i class="far fa-envelope"></i></span>
        </div>
        <div class="col-xs-10 col-sm-10 cta-info">
          <h4>Do you have any question?</h4>
          <p>email.example@gmail.com</p>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-4 cta-tab">
      <div class="col-xs-12 col-sm-12">
        <div class="col-xs-2 col-sm-2 no-pad-lr cta-icon">
          <span><i class="fas fa-phone-volume"></i></span>
        </div>
        <div class="col-xs-10 col-sm-10 cta-info">
          <h4>456-456-8955</h4>
          <p>email.example@gmail.com</p>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-4 cta-tab">
      <div class="col-xs-12 col-sm-12">
        <button class="main-btn cta-btn">Contact Us</button>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 footer-top">
    <div class="col-xs-12 col-sm-3 widget-tab first-widget-tab">
      <div class="col-xs-12 col-sm-12 no-pad-lr ">
        <img src="assets/images/logo2.png" class="img-responsive logo">
        <p class="address-info">Po Box 12666 Collin Street, Australia</p>
        <p class="phone-info">(+91) 1234-567-8910</p>
        <p class="email-info">info@company.com</p>
        <p class="social-icons">
          <a><i class="fab fa-facebook-f"></i></a>
          <a><i class="fab fa-twitter"></i></a>
          <a><i class="fab fa-instagram"></i></a>
          <a><i class="fab fa-github"></i></a>
          <a><i class="fab fa-behance"></i></a>
        </p>
      </div>
    </div>
    <div class="col-xs-12 col-sm-3 widget-tab second-widget-tab">
      <h3 class="footer-head">Pages</h3>
      <a href="">About Us</a>
      <a href="">Delivery Information</a>
      <a href="">Privacy Policy</a>
      <a href="">Terms</a>
      <a href="">Faq</a>
      <a href="contact-us.php">Contact Us</a>
    </div>
    <div class="col-xs-12 col-sm-3 widget-tab third-widget-tab">
      <h3 class="footer-head">Account</h3>
      <a href="overview.php">My Account</a>
      <a href="cart.php">Cart</a>
      <a href="order-history.php">Order History</a>
      <a href="wishlist.php">Wishlist</a>
      <a href="">Newsletter</a>
      <a href="">Returns</a>
    </div>
    <div class="col-xs-12 col-sm-3 widget-tab fourth-widget-tab">
      <h3>Insta Gallery</h3>
      <div class="col-xs-12 col-sm-12 no-pad-lr footer-insta-gallery">
        <div class="col-xs-4 col-sm-4">
          <img src="assets/images/products/product1.jpeg" class="img-responsive">
        </div>
        <div class="col-xs-4 col-sm-4">
          <img src="assets/images/products/product2.jpeg" class="img-responsive">
        </div>
        <div class="col-xs-4 col-sm-4">
          <img src="assets/images/products/product3.jpeg" class="img-responsive">
        </div>
        <div class="col-xs-4 col-sm-4">
          <img src="assets/images/products/product4.jpeg" class="img-responsive">
        </div>
        <div class="col-xs-4 col-sm-4">
          <img src="assets/images/products/product5.jpeg" class="img-responsive">
        </div>
        <div class="col-xs-4 col-sm-4">
          <img src="assets/images/products/product6.jpeg" class="img-responsive">
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 footer-bottom text-center">
    <p>Copyright @ 2018, Your Store, All Right Reserved.</p>
  </div>
</div>