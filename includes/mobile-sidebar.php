<div class="col-xs-12 mobile-navbar no-pad-lr hidden-sm hidden-md hidden-lg">
    <div class="col-xs-12"><button class="close-sidebar"><i class="fas fa-times"></i></button></div>
    <div class="sidebar">
        <div id="leftside-navigation" class="nano">
          <ul class="nano-content">
            <li>
              <div class="mobile-sidebar-header">
                <a href="wishlist.php" title="Wishlist" class="wishlist pull-left"><i class="far fa-heart"></i></a>
                <a href="overview.php" class="user-account" title="Account"><i class="far fa-user"></i></a>
                <a href="cart.php" title="Cart" class="cart pull-right"><i class="fas fa-shopping-basket"></i><span class="badge">0</span></a>                            
              </div>
            </li>
            <li class="mobile-sub-menu">
              <a href="javascript:void(0);"><span>Mens</span><i class="arrow fa fa-angle-right pull-right"></i></a>
              <ul>
                <li><a href="products.php">Jeans</a>
                </li>
                <li><a href="products.php">Pants</a>
                </li>
                <li><a href="products.php">Tops</a>
                </li>
                <li><a href="products.php">Hat</a>
                </li>
                <li><a href="products.php">Accesories</a>
                </li>
              </ul>
            </li>
            <li class="mobile-sub-menu">
              <a href="javascript:void(0);"><span>Womens</span><i class="arrow fa fa-angle-right pull-right"></i></a>
              <ul>
                <li><a href="products.php">Jeans</a>
                </li>
                <li><a href="products.php">Pants</a>
                </li>
                <li><a href="products.php">Tops</a>
                </li>
                <li><a href="products.php">Hat</a>
                </li>
                <li><a href="products.php">Accesories</a>
                </li>
              </ul>
            </li>
            <li class="mobile-sub-menu">
              <a href="javascript:void(0);"><span>Kids</span><i class="arrow fa fa-angle-right pull-right"></i></a>
              <ul>
                <li><a href="products.php">Jeans</a>
                </li>
                <li><a href="products.php">Pants</a>
                </li>
                <li><a href="products.php">Tops</a>
                </li>
                <li><a href="products.php">Hat</a>
                </li>
                <li><a href="products.php">Accesories</a>
                </li>
              </ul>
            </li>
            <li class="mobile-sub-menu">
              <a href="javascript:void(0);"><span>Accessories</span><i class="arrow fa fa-angle-right pull-right"></i></a>
              <ul>
                <li><a href="products.php">Goggles</a>
                </li>
                <li><a href="products.php">Bags</a>
                </li>
                <li><a href="products.php">Jewellery</a>
                </li>
                <li><a href="products.php">Hat</a>
                </li>
                <li><a href="products.php">Belts</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
    </div>
</div>