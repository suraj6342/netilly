<div class="navbar">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 col-sm-12 nav-inner">
          <div class="col-xs-12 col-sm-4 search-bar">
            <form class="col-xs-12 col-sm-12 no-pad-lr">
              <div>
                  <input type="text" name="" placeholder="Search">
                  <button class="btn search-submit">
                      <img src="assets/images/icon/search.svg">
                  </button>
              </div>
            </form>
          </div>
          <div class="col-xs-12 col-sm-4 brand-logo">
            <a href="index.php"><img src="assets/images/logo2.png" class="img-responsive"></a>
            <div class="hidden-sm hidden-md hidden-lg mobile-nav-menu">
              <div class="menu-wrapper">
                <div class="hamburger-menu"></div>    
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 text-right navbar-right-tab hidden-xs">
               
                <?php

                if (!empty($_SESSION['user_id'])) {
                  echo $_SESSION['user_name'].'<a href="logout.php">( Logout )</a>';
                  

                }else{
                  echo '<a href="overview.php" class="user-account" title="Account"><i class="far fa-user"></i> &nbspLogin</a>';
                }

                ?>
               <a href="cart.php" title="Cart" class="cart">Cart</a>
          </div>
        </div>
      </div>
    </div>
</div>