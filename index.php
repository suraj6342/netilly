<?php
include_once 'config/config.php'; // All Settings
include_once 'validation/classes/product.php'; //Object
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Web Design</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>
    
    <link rel="stylesheet" type="text/css" href="assets/css/index.css?ver=<?php echo $randStr; ?>">    
  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>

    <div class="main-wrapper">
      <div class="col-xs-12 col-sm-12 no-pad-lr product-tabs-main">
        <div class="col-xs-12 col-sm-12 product-tabs-main-head text-center">
          <h2>Featured Collections</h2>
          <p>vestibulum feugiat quam et sem bibendum ac</p>
        </div>
        <div class="col-xs-12 col-sm-12 product-tabs-main-tab">
          <div>
            <div class="col-xs-12 col-sm-12 no-pad-lr">
                    <?php
                    /* instantiate Database */
                    $database   = new Database();
                    $db         = $database->getConnection();

                    $product = new GetProduct($db); //CLASS INI
                    $productRes = $product->getallproduct();
                    $productResData  =  json_decode($productRes);
                    //print_r($productResData->productData);

                    foreach ($productResData->productData as $data) { ?>
                      <div class="col-xs-12 col-sm-3">
                        <div class="col-xs-12 col-sm-12 no-pad-lr product-tab">
                            <a href="product-description.php?productid=<?=$data->id;?>">
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-top">
                                    <img src="assets/images/products/product1.jpeg" class="img-responsive">
                                </div>
                                <div class="col-xs-12 col-sm-12 no-pad-lr product-tab-bottom">
                                    <h4 class="text-center"><?=$data->title;?></h4>
                                    <span class="list-price pull-left">Polo Shirts</span>
                                    <span class="price pull-right">Rs. <?=$data->price;?></span>
                                    
                                </div>
                            </a>
                        </div>
                      </div>
                    <?php  }  ?>
            </div>
          </div>
        </div>
      </div>
      
    </div>
    <?php require_once 'includes/script.php'; ?>
    <script type="text/javascript" src="assets/js/index.js?ver=<?php echo $randStr; ?>"></script>
  </body>
</html>