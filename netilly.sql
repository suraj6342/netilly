-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 05, 2020 at 12:07 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `netilly`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `customer_id` int(250) NOT NULL,
  `items` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  `paid` int(10) NOT NULL,
  `shipped` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `customer_id`, `items`, `expire_date`, `paid`, `shipped`) VALUES
(25, 75, '[{\"id\":\"4\",\"size\":\"big\",\"quantity\":\"6\"},{\"id\":\"2\",\"size\":\"small\",\"quantity\":6},{\"id\":\"2\",\"size\":\"big\",\"quantity\":5}]', '2021-01-04 04:14:12', 0, 0),
(26, 70, '[{\"id\":\"2\",\"size\":\"small\",\"quantity\":\"2\"},{\"id\":\"3\",\"size\":\"small\",\"quantity\":9}]', '2021-01-04 04:19:03', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(100) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `sizes` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `price`, `image`, `description`, `sizes`) VALUES
(1, 'Product 1', '1000.00', 'assets/productimg/product1.jpeg,assets/productimg/product2.jpeg,assets/productimg/product3.jpeg,assets/productimg/product4.jpeg,assets/productimg/product5.jpeg', 'lorem ipsum jksdc jnasdc jknasdc jnkasdc jknsdc\r\nklmskdc\r\nklmlsdc\r\nklmsdc\r\n', 'big:20,small:20,medium:30'),
(2, 'Product 2', '500.00', 'assets/productimg/product1.jpeg,assets/productimg/product2.jpeg,assets/productimg/product3.jpeg,assets/productimg/product4.jpeg,assets/productimg/product5.jpeg', 'lorem ipsum jksdc jnasdc jknasdc jnkasdc jknsdc\r\nklmskdc\r\nklmlsdc\r\nklmsdc\r\n', 'big:20,small:20,medium:30'),
(3, 'Product 3', '600.00', 'assets/productimg/product1.jpeg,assets/productimg/product2.jpeg,assets/productimg/product3.jpeg,assets/productimg/product4.jpeg,assets/productimg/product5.jpeg', 'lorem ipsum jksdc jnasdc jknasdc jnkasdc jknsdc\r\nklmskdc\r\nklmlsdc\r\nklmsdc\r\n', 'big:20,small:20,medium:30'),
(4, 'Product 4', '700.00', 'assets/productimg/product1.jpeg,assets/productimg/product2.jpeg,assets/productimg/product3.jpeg,assets/productimg/product4.jpeg,assets/productimg/product5.jpeg', 'lorem ipsum jksdc jnasdc jknasdc jnkasdc jknsdc\r\nklmskdc\r\nklmlsdc\r\nklmsdc\r\n', 'big:20,small:20,medium:30'),
(5, 'Product 5', '800.00', 'assets/productimg/product1.jpeg,assets/productimg/product2.jpeg,assets/productimg/product3.jpeg,assets/productimg/product4.jpeg,assets/productimg/product5.jpeg', 'lorem ipsum jksdc jnasdc jknasdc jnkasdc jknsdc\r\nklmskdc\r\nklmlsdc\r\nklmsdc\r\n', 'big:20,small:20,medium:30'),
(6, 'Product 6', '900.00', 'assets/productimg/product1.jpeg,assets/productimg/product2.jpeg,assets/productimg/product3.jpeg,assets/productimg/product4.jpeg,assets/productimg/product5.jpeg', 'lorem ipsum jksdc jnasdc jknasdc jnkasdc jknsdc\r\nklmskdc\r\nklmlsdc\r\nklmsdc\r\n', 'big:20,small:20,medium:30'),
(7, 'Product 7', '1100.00', 'assets/productimg/product1.jpeg,assets/productimg/product2.jpeg,assets/productimg/product3.jpeg,assets/productimg/product4.jpeg,assets/productimg/product5.jpeg', 'lorem ipsum jksdc jnasdc jknasdc jnkasdc jknsdc\r\nklmskdc\r\nklmlsdc\r\nklmsdc\r\n', 'big:20,small:20,medium:30'),
(8, 'Product 8', '1200.00', 'assets/productimg/product1.jpeg,assets/productimg/product2.jpeg,assets/productimg/product3.jpeg,assets/productimg/product4.jpeg,assets/productimg/product5.jpeg', 'lorem ipsum jksdc jnasdc jknasdc jnkasdc jknsdc\r\nklmskdc\r\nklmlsdc\r\nklmsdc\r\n', 'big:20,small:20,medium:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(100) NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`, `mobile`, `address`, `shipping_address`) VALUES
(58, 'Suraj Rawat', 'suraj63421@gmail.com', 'a66abb5684c45962d887564f08346e8d', '07011353015', '', ''),
(75, 'ABC XYZ', 'abc@gmail.com', '25f9e794323b453885f5181f1b624d0b', '07011353015', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
