<?php
    require_once "config/settings.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Your Account</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>    
    <link rel="stylesheet" type="text/css" href="assets/css/overview-wishlist-cart-common.css?ver=<?php echo $randStr; ?>">
    <link rel="stylesheet" type="text/css" href="assets/css/overview.css?ver=<?php echo $randStr; ?>">

  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/mobile-sidebar.php'; ?>

    <div class="col-xs-12 col-sm-12 no-pad-lr main-wrapper">
        <div class="col-xs-12 col-sm-12 tabs-main">
            <div class="col-xs-12 col-sm-12 account-login-tab">
              <div class="col-xs-12 col-sm-12 no-pad-lr account-login-tab-inner">
                <div class="col-xs-12 col-sm-12 text-center account-login-head">
                    <h4>Sign In</h4>
                    <hr class="head-line">
                    <p class="text-danger text-center" id="main-error-message"></p>
                </div>
                <div class="col-xs-12 col-sm-6 sign-in-tab">
                    <h4>Returning Customer</h4>
                    <hr class="head-line">
                    <form id="login_form" method="POST" enctype="multipart/form-data" novalidate>
                        <div class="col-xs-12 col-sm-12 no-pad-lr sign-in-tab-inputs">
                            <input type="text" name="logemail" placeholder="Email Address" class="main-input">
                            <span class="text-danger err_mes_tab" id="logemail_err"></span>
                            <input type="password" name="logpassword" placeholder="Password" class="main-input">
                            <span class="text-danger err_mes_tab" id="logpassword_err"></span>
                        </div>
                    <div class="col-xs-12 col-sm-12 no-pad-lr text-center login-btn-tab">
                        <button type="submit" class="main-btn">Login</button>
                    </div>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-6 sign-up-tab">
                    <h4>Create An Account</h4>
                    <hr class="head-line">
                    <p class="text-danger text-center" id="main-error-message"></p>
                    <form id="signup_form" method="POST" enctype="multipart/form-data" novalidate>
                        <div class="col-xs-12 col-sm-12 sign-up-input-tab no-pad-lr">
                              <input type="text" name="name" placeholder="Full Name" class="main-input">
                              <span class="text-danger err_mes_tab" id="name_err"></span>
                        </div>
                        <div class="col-xs-12 col-sm-12 no-pad-lr sign-up-input-tab">
                            <input type="text" name="email" placeholder="Email Address" class="main-input">
                            <span class="text-danger err_mes_tab" id="email_err"></span>
                        </div>
                        <div class="col-xs-12 col-sm-12 no-pad-lr sign-up-input-tab">
                            <input type="text" name="phone" placeholder="Phone Number" class="main-input">
                            <span class="text-danger err_mes_tab" id="phone_err"></span>
                        </div>
                        <div class="col-xs-12 col-sm-12 no-pad-lr sign-up-input-tab">
                              <input type="password" name="password" placeholder="Password" class="main-input">
                              <span class="text-danger err_mes_tab" id="password_err"></span>
                        </div>
                        <div class="col-xs-12 col-sm-12 no-pad-lr text-center create-account-btn">
                          <button type="submit" class="main-btn">Create Account</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>

        <!-- update profile Modal -->
    </div>

    <?php require_once 'includes/script.php'; ?>
    <script type="text/javascript">
        $('#signup_form').submit(function(e){
            e.preventDefault();
                var formData = new FormData($(this)[0]);

                $.ajaxSetup({
                    url: "validation/signup.php",
                    data: formData,
                    async: true,
                    cache: false,
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                });
                $.post()
                .done(function(response) {
                    var res = JSON.parse(response);
                    var status = res['status'];
                    var message = res['message'];
                    var error = res['error'];

                    if ( status == 'success' ){
                        location.reload();
                    }
                    else{
                        if(Object.keys(error).length > 0)
                        {

                            $('.err_mes_tab').html("");                            
                            for (x in error)
                            {
                                $('#'+x+'_err').html('('+error[x]+')');
                            }
                        }
                        $('#main-error-message').show();
                        $('#main-error-message').html(message);
                    }
                })
                .fail(function() {
                    alert('failed to process');
                })
                return false;
        });


        $('#login_form').submit(function(e){
            e.preventDefault();
                var formData = new FormData($(this)[0]);

                $.ajaxSetup({
                    url: "validation/login.php",
                    data: formData,
                    async: true,
                    cache: false,
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                });
                $.post()
                .done(function(response) {
                    var res = JSON.parse(response);
                    var status = res['status'];
                    var message = res['message'];
                    var error = res['error'];

                    if ( status == 'success' ){
                        window.location.replace("index.php");
                    }
                    else{
                        if(Object.keys(error).length > 0)
                        {

                            $('.err_mes_tab').html("");                            
                            for (x in error)
                            {
                                $('#'+x+'_err').html('('+error[x]+')');
                            }
                        }
                        $('#main-error-message').show();
                        $('#main-error-message').html(message);
                    }
                })
                .fail(function() {
                    alert('failed to process');
                })
                return false;
        });

    </script>

  </body>
</html>