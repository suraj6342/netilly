<?php
include_once 'config/config.php'; // All Settings
include_once 'validation/classes/product.php'; //Object

$id = $_REQUEST['productid'];
$proId = (int)$id;

$database   = new Database();
$db         = $database->getConnection();

$product = new GetProduct($db); //CLASS INI
$productRes = $product->getallproductByid($proId);
$productResData  =  json_decode($productRes);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Product Description</title>
    <?php include 'includes/meta-tag.php'; ?>
    <?php include 'includes/style.php'; ?>

    <link rel="stylesheet" type="text/css" href="assets/css/product-desc.css?ver=<?php echo $randStr; ?>">
    
  </head>
  <body>
    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/mobile-sidebar.php'; ?>

    <div class="main-wrapper">

      <div class="col-xs-12 col-sm-12 no-pad-lr product-description-main"> 
        <div class="col-xs-12 col-sm-6 no-pad-lr product-description-left">
            <div class="col-xs-12 col-sm-12 no-pad-lr">
                <div id="product-desc-slider" class="carousel slide" data-ride="carousel" data-interval="false">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                      <?php if($productResData->productData->image != ''){
                        $imno = 0;
                        $images = explode(',', $productResData->productData->image);

                        foreach($images as $image){ ?>
                          <li data-target="#product-desc-slider" data-slide-to="<?=$imno;?>">
                            <div class="thumbnail">
                                <img src="<?=$image;?>" alt="image1">
                            </div>
                          </li>
                      <?php  
                      $imno++;
                       }
                      } ?>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox" id="product_slider">
                      <?php if($productResData->productData->image != ''){
                        $imno = 0;
                        $images = explode(',', $productResData->productData->image);

                        foreach($images as $image){ ?>
                          <div class="item">
                            <img src="<?=$image;?>">
                          </div>
                      <?php  
                      $imno++;
                       }
                      } ?>

                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#product-desc-slider" role="button" data-slide="prev">
                      <span class="fas fa-chevron-left" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#product-desc-slider" role="button" data-slide="next">
                      <span class="fas fa-chevron-right" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 no-pad-lr product-description-right">
            <div class="col-xs-12 col-sm-12 no-pad-lr">
              <h2 class="product_title"><?=$productResData->productData->title;?></h2>
              <p class="product_price"><?=$productResData->productData->price;?></p>
              <p class="product_desc"><?=$productResData->productData->description;?></p>
              <form method="post" id="add_product_form" enctype="multipart/form-data" novalidate>
                <div class="size_pick">
                  <p>Sizes</p>
                  <p class="text-danger err_mes_tab" id="size_err"></p>
                  <input type="text" class="hidden" name="product_id" id="product_id" value="<?=$productResData->productData->id;?>">
                  <input type="text" class="hidden" name="customer_id" id="user_id" value="<?=$_SESSION['user_id'];?>">

                  <select id="size" class="form-control" name="size">
                      <option value="">Select Size</option>
                      <?php 
                        $size_Array =array();
                        $size_Array = explode(',', $productResData->productData->sizes);
                        foreach ($size_Array as $ss ) {
                          $s =explode(':', $ss);
                          $size= $s[0];
                          $available = $s[1];
                            echo '<option value="'.$size.'">'.$size.' ('.$available.' Available)</option>';   
                            
                        }
                      ?>
                  </select>

                </div>
                <div class="size_pick">
                  <p>Quantity</p>
                  <p class="text-danger err_mes_tab" id="quantity_err"></p>
                  <input type="number" min="1" id="quantity" name="quantity" class="input-btn">
                </div>
                <?php

                if (!empty($_SESSION['user_id'])) {
                  echo '<div class="add-cart-btn">
                  <button class="btn main-btn" type="submit">Add To Cart</button>
                </div>';
                 
                }else{
                  echo 'Please <a href="overview.php">Login</a> first to add the Product in cart'; 
                }

                ?>              
                
              </form>
            </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 no-pad-lr product-description-para">
        <div class="col-xs-12 col-sm-12 no-pad-lr product-description-inner">
          <h2>Description</h2>
          <p><?=$productResData->productData->description;?></p>
        </div>
      </div>
    </div>

    <?php require_once 'includes/script.php'; ?>
    <script type="text/javascript" src="assets/js/product-desc.js?ver=<?php echo $randStr; ?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
          $("#product_slider .item:first").addClass("active");
        });



        $('#add_product_form').submit(function(e){
            e.preventDefault();
                var formData = new FormData($(this)[0]);

                $.ajaxSetup({
                    url: "validation/addcart.php",
                    data: formData,
                    async: true,
                    cache: false,
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                });
                $.post()
                .done(function(response) {
                    var res = JSON.parse(response);
                    var status = res['status'];
                    var message = res['message'];
                    var error = res['error'];
                    //alert(status);
                    if ( status == 'success' ){
                        location.reload();
                    }
                    else{
                        if(Object.keys(error).length > 0)
                        {
                            $('.err_mes_tab').html("");                            
                            for (x in error)
                            {
                                $('#'+x+'_err').html('('+error[x]+')');
                            }
                        }
                    }
                })
                .fail(function() {
                    alert('failed to process');
                })
                return false;
        });


    </script>



  </body>
</html>