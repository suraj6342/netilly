<?php 

include_once '../config/config.php';
include_once 'classes/product.php';
include_once 'classes/validation.php';

$error = array();
$status = $message = "";

$size = ( isset($_POST['size']) ?  $_POST['size'] : '' );
$quantity = ( isset($_POST['quantity']) ?  $_POST['quantity'] : '' );
$product_id = ( isset($_POST['product_id']) ?  $_POST['product_id'] : '' );
$customer_id = ( isset($_POST['customer_id']) ?  $_POST['customer_id'] : '' );

/* instantiate Database */
$database   = new Database();
$db         = $database->getConnection();

$validation = new Validation();

$size_emp_res = $validation->emptycheck($size);

if($size_emp_res == true) {
	$error['size'] = 'Please Select Size';
}

$quantity_emp_res = $validation->emptycheck($size);

if($quantity_emp_res == true) {
	$error['quantity'] = 'Please Enter Quantity';
}


if(!empty($error)) {
	$status = "fail";
	$message = "Error Found";
}else{

	$item[] = array(
		'id'		=> $product_id,
		'size'		=> $size,
		'quantity'	=> $quantity,
	);
	$domain = ($_SERVER['HTTP_HOST'] != 'localhost')?'.'.$_SERVER['HTTP_HOST']:false;

	//check to see if the cart cookie exists
	if ($cart_id != '') {
		$cartQ = $db->query("SELECT * FROM cart WHERE id ='{$cart_id}'");
		$cart = mysqli_fetch_assoc($cartQ);
		$previous_items = json_decode($cart['items'],true);
		$item_match = 0;
		$new_items = array();
		foreach ($previous_items as $pitem) {
			if ($item[0]['id'] == $pitem['id'] && $item[0]['size'] == $pitem['size']) {
				$pitem['quantity'] = $pitem['quantity'] + $item[0]['quantity'];
				$item_match = 1;
			}
			$new_items[] = $pitem;
		}
		if ($item_match != 1) {
			$new_items = array_merge($item,$previous_items);
		}
		$items_json = json_encode($new_items);
		$cart_expire = date("Y-m-d H:i:s" ,strtotime("+30 days"));
		$db->query("UPDATE cart SET items = '{$items_json}',expire_date = '{$cart_expire}' WHERE id ='{$cart_id}' ");
		setcookie(CART_COOKIE,'',1,"/",$domain,false);
		setcookie(CART_COOKIE,$cart_id,CART_COOKIE_EXPIRE,'/',$domain,false);
		$status ="success";

	}else{
		//add the cart to the database and set cookie
		$item_json = json_encode($item);
		$cart_expire = date("Y-m-d H:i:s",strtotime("+30 days"));
		$cartInsertQuery = $db->query("INSERT INTO cart (items,expire_date) VALUES ('$item_json','$cart_expire' ) ");
		if ($cartInsertQuery) {
			$cart_id = $db->insert_id;
		}

		setcookie(CART_COOKIE,$cart_id,CART_COOKIE_EXPIRE,'/',$domain,false);
		$status ="success";
	 }

}


$obj = new stdClass();
$obj->status = $status;
$obj->message = $message;
$obj->error = $error;
echo json_encode($obj);

