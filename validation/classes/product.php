<?php

class GetProduct{
 
    // database connection and table name
    private $conn;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

	function getallproduct(){

		$productArr = array();		
		$getProduct = "SELECT * FROM products ORDER BY id ASC ";
		
		$getProductQuery = $this->conn->query($getProduct);
		$productCount = mysqli_num_rows($getProductQuery);

		if ($productCount > 0) {
        	$status = "success";
			$message = "Product found.";
			 while($productData = mysqli_fetch_assoc($getProductQuery)){
			 	$productArr[] = $productData;
			 }

		}else{
        	$status = "fail";
			$message = "User not found.";
		}

		$obj = new stdClass();
		$obj->status = $status;
		$obj->message = $message;
		$obj->productData = $productArr;
		return json_encode($obj);

	}

	function getallproductByid($id){

		$proId = $id;

		$getProductById = "SELECT * FROM products WHERE id = '$proId' ";
		
		$getProductByIdQuery = $this->conn->query($getProductById);
		$getProductByCount = mysqli_num_rows($getProductByIdQuery);

		if ($getProductByCount > 0) {
        	$status = "success";
			$message = "Product found.";
			 $productDataById = mysqli_fetch_assoc($getProductByIdQuery);

		}else{
        	$status = "fail";
			$message = "User not found.";
		}

		$obj = new stdClass();
		$obj->productData = $productDataById;
		return json_encode($obj);

	}

	function addToCart(){
		
	}


}

?>