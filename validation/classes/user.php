<?php

class User{
 
    // database connection and table name
    private $conn;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

	function signup($params){
		
		$status = $message = "";
        
        $user_details = array();
		$json = json_decode($params);

		$name = $this->filter($json->name);
		$email = $this->filter($json->email);
		$phone = $this->filter($json->phone);
		$hashpassword = md5($this->filter($json->password));

		$insertuser = "INSERT INTO users ( full_name, email, password, mobile ) values ( '$name', '$email', '$hashpassword', '$phone' ) ";
		
		$insertuserquery = $this->conn->query($insertuser);
		
		if ($insertuserquery) {

			$userid = $this->conn->insert_id;

        	$status = "success";
			$message = "SignUp Successfull.";

	        $_SESSION['user_id'] = $userid;
	        $_SESSION['user_name'] = $name;
	        $_SESSION['user_email'] = $email;

		}else{
        	$status = "fail";
			$message = "Error S1 -> ".mysqli_error($db);
		}

		$obj = new stdClass();
		$obj->status = $status;
		$obj->message = $message;
		return json_encode($obj);

	}

	function login($params){

		$status = $message = "";
		$userdata = array();

        $user_details = array();
		$json = json_decode($params);

		$email = $this->filter($json->email);
		$hashpassword = md5($this->filter($json->password));


		$getUser = "SELECT * FROM users WHERE email = '$email' AND password = '$hashpassword' ";
		
		$getUserQuery = $this->conn->query($getUser);
		$userCount = mysqli_num_rows($getUserQuery);
		$userData = mysqli_fetch_assoc($getUserQuery);
		
		if ($userCount > 0) {
        	$status = "success";
			$message = "User found.";
			
	        $_SESSION['user_id'] = $userData['id'];
	        $_SESSION['user_name'] = $userData['full_name'];
	        $_SESSION['user_email'] = $userData['email'];
					
		}else{
        	$status = "fail";
			$message = "User not found.";
		}

		$login_res_object = new stdClass();
		$login_res_object->status = $status;
		$login_res_object->message = $message;

		return json_encode($login_res_object);
	}

	function user_check($email){
        $returnVal = false;

		$email_id = $email;
		$checkUser = "SELECT * FROM users WHERE email = '$email_id' ";
		
		$checkUserQuery = $this->conn->query($checkUser);
		$userCount = mysqli_num_rows($checkUserQuery);
		
		if ($userCount > 0) {
	        $returnVal = true;
		}
		return $returnVal;

	}

	function isUserLogin(){

	}

	// Senitize/Filter data
	function filter($data){
		return trim(strip_tags(htmlspecialchars($data)));
	}

}