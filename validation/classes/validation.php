<?php

class Validation{

    public function emptycheck($fieldValue)
    {
        $returnVal = false;
        if(empty($fieldValue))
            $returnVal = true;
        return $returnVal;
    }

    public function namePatternCheck($fieldValue)
    {
        $returnVal = false;
        if( !preg_match("^[\\p{L} .'-]+$^", $fieldValue) )
            $returnVal = true;
        
        return $returnVal;
    }

    public function emailPatternCheck($fieldValue)
    {
        $returnVal = false;
        if (!filter_var($fieldValue, FILTER_VALIDATE_EMAIL)) {
            $returnVal = true;
        }
        return $returnVal;
    }

    public function phonePatternCheck($fieldValue)
    {
        $returnVal = false;
        if( !preg_match('/^[+0-9*]{6,15}$/', $fieldValue) )
            $returnVal = true;
        return $returnVal;
    }

    public function passLengthCheck($fieldValue)
    {
        $returnVal = false;
        if( strlen($fieldValue) < 6 )
            $returnVal = true;
        return $returnVal;
    }

}


?>