<?php 

include_once '../config/config.php';      // All Settings
include_once 'classes/user.php';             // Object
include_once 'classes/validation.php';           // Object

$error = array();
$status = $message = "";



$logemail = ( isset($_POST['logemail']) ?  $_POST['logemail'] : '' );
$logpassword = ( isset($_POST['logpassword']) ?  $_POST['logpassword'] : '' );


/* instantiate Database */
$database   = new Database();
$db         = $database->getConnection();

$user = new User($db); //CLASS INI
$validation = new Validation();


$email_emp_res = $validation->emptycheck($logemail);
if($email_emp_res == true) {
	$error['logemail'] = 'Email cant be empty.';
}else{
	$email_pattern_res = $validation->emailPatternCheck($logemail); 
	if($email_pattern_res == true) {
		$error['logemail'] = 'Enter valid email address.';
	}
}

$pass_emp_res = $validation->emptycheck($logpassword);
if($pass_emp_res == true) {
	$error['logpassword'] = 'Password cant be empty.';
}

if(!empty($error)) {
	$status = "fail";
	$message = "Error Found";
}else{


 	$params = array(
        'email' 	=>   $logemail,
        'password'  =>   $logpassword,
    );

    $userData = json_encode($params);
	$user_signup_res = $user->login($userData);	
	
	$signup_res  =  json_decode($user_signup_res);


	if ($signup_res->status == "success" ) {
		$status = "success";
		$message = $signup_res->message;
	}else{
		$status = "fail";
		$message = $signup_res->message;
	}
}


$obj = new stdClass();
$obj->status = $status;
$obj->message = $message;
$obj->error = $error;
echo json_encode($obj);

