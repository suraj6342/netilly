<?php 

include_once '../config/config.php';      // All Settings
include_once 'classes/user.php';             // Object
include_once 'classes/validation.php';           // Object

$error = array();
$status = $message = "";



$name = ( isset($_POST['name']) ?  $_POST['name'] : '' );
$last_name = ( isset($_POST['last_name']) ?  $_POST['last_name'] : '' );
$email = ( isset($_POST['email']) ?  $_POST['email'] : '' );
$phone = ( isset($_POST['phone']) ?  $_POST['phone'] : '' );
$password = ( isset($_POST['password']) ?  $_POST['password'] : '' );


/* instantiate Database */
$database   = new Database();
$db         = $database->getConnection();

$user = new User($db); //CLASS INI
$validation = new Validation();

$name_emp_res = $validation->emptycheck($name);
if($name_emp_res == true) {
	$error['name'] = 'Name can not be empty.';
}else{
	$name_pattern_res = $validation->namePatternCheck($name); 
	if ($name_pattern_res == true) {
		$error['name'] = 'Enter valid name.';
	}
}

$email_emp_res = $validation->emptycheck($email);
if($email_emp_res == true) {
	$error['email'] = 'Email cant be empty.';
}else{
	$email_pattern_res = $validation->emailPatternCheck($email); 
	if($email_pattern_res == true) {
		$error['email'] = 'Enter valid email address.';
	}else{
		$user_res = $user->user_check($email);
		if($user_res == true) {
			$error['email'] = 'This email already exist.';
		}
	}
}

$phone_emp_res = $validation->emptycheck($phone);
if($phone_emp_res == true) {
	$error['phone'] = 'Phone cant be empty.';
}else{
	$phone_pattern_res = $validation->phonePatternCheck($phone);
	if($phone_pattern_res == true) {
		$error['phone'] = 'Enter valid phone number.';
	}
}

$pass_emp_res = $validation->emptycheck($password);
if($pass_emp_res == true) {
	$error['password'] = 'Password cant be empty.';
}else{
	$pass_length_res = $validation->passLengthCheck($password);
	if($pass_length_res == true) {
		$error['password'] = 'Password length must be greater than 6 characters.';
	}	
}

if(!empty($error)) {
	$status = "fail";
	$message = "Error Found";
}else{


 	$params = array(
        'name'  	=>   $name,
        'email' 	=>   $email,
        'phone' 	=>   $phone,
        'password'  =>   $password,
    );

    $userData = json_encode($params);
	$user_signup_res = $user->signup($userData);	
	
	$signup_res  =  json_decode($user_signup_res);


	if ($signup_res->status == "success" ) {
		$status = "success";
		$message = $signup_res->message;
	}else{
		$status = "fail";
		$message = $signup_res->message;
	}
}


$obj = new stdClass();
$obj->status = $status;
$obj->message = $message;
$obj->error = $error;
echo json_encode($obj);

